import datetime
import gi
from menu import MENU_XML

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib  # noqa: E402 pylint: disable=wrong-import-position,wrong-import-order
from widgets import RadioGroup  # noqa: E402 pylint: disable=wrong-import-position,wrong-import-order


class ScreenLock:

    screensaver_settings = Gio.Settings.new('org.gnome.desktop.screensaver')
    desktop_session_settings = Gio.Settings.new('org.gnome.desktop.session')
    original_idle_delay = None

    @property
    def is_enabled(self):
        return self.screensaver_settings.get_boolean('lock-enabled')

    def disable(self):
        self.screensaver_settings.set_boolean('lock-enabled', False)
        self.original_idle_delay = self.desktop_session_settings.get_value('idle-delay')
        self.desktop_session_settings.set_uint('idle-delay', 0)

    def enable(self):

        self.screensaver_settings.set_boolean('lock-enabled', True)

        if self.desktop_session_settings.get_uint('idle-delay') == 0:
            if self.original_idle_delay:
                self.desktop_session_settings.set_value('idle-delay', self.original_idle_delay)
            else:
                self.desktop_session_settings.set_uint('idle-delay', 300)


screenlock = ScreenLock()


class LockFreeWindow(Gtk.ApplicationWindow):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, title="LockFree", **kwargs)

        self._seconds_left = 0

        self._init_window()
        self._init_widgets()
        self._init_signal_listeners()

        self._add_main_ui()

        self.show_all()

    def _init_window(self):
        self.set_border_width(25)

    def _init_widgets(self):

        self._main_ui_stack = Gtk.Stack()

        self._quantity_input = Gtk.SpinButton.new_with_range(min=1, max=600, step=1)
        self._quantity_input.set_value(1)

        self._unit_radio_group = RadioGroup({
            'minutes': 'minutes',
            'hours': 'hours'
        })

        self._time_left_label = Gtk.Label()

    def _init_signal_listeners(self):
        self._quantity_input.connect('activate', self._quantity_input_enter)
        self._unit_radio_group.connect('activate', self._unit_radio_enter)

    def _quantity_input_enter(self, input_widget):
        self._start_temp_screenlock_disable()

    def _unit_radio_enter(self, radio):
        self._start_temp_screenlock_disable()

    def start(self):
        self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)
        self.show_all()
        self._reinit_main_ui_stack()
        self.connect('destroy', self.exit)
        Gtk.main()

    def _add_main_ui(self):
        self._main_ui_stack.add_named(self._screen_lock_ui, 'main')
        self._main_ui_stack.add_named(self._timer_ui, 'timer')
        self._main_ui_stack.add_named(self._already_disabled_ui, 'already_disabled')
        self.add(self._main_ui_stack)

    def _reinit_main_ui_stack(self):
        if self._seconds_left > 0:
            self._main_ui_stack.set_visible_child_name('timer')
        elif not screenlock.is_enabled:
            self._main_ui_stack.set_visible_child_name('already_disabled')
        else:
            self._main_ui_stack.set_visible_child_name('main')

    @property
    def _screen_lock_ui(self):
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box.add(self._header_label)
        box.add(self._time_picker)
        box.add(self._start_button)
        return box

    @property
    def _header_label(self):
        header_label = Gtk.Label()
        header_label.set_halign(Gtk.Align.START)
        header_label.set_markup("<b>Disable the screen lock for:</b>")
        return header_label

    @property
    def _time_picker(self):
        box = Gtk.Box()
        box.add(self._quantity_input)
        box.add(self._unit_radio_group)
        return box

    @property
    def _start_button(self):
        button = Gtk.Button.new_with_label("Start")
        button.connect('clicked', self._on_start_button_click)
        return button

    def _on_start_button_click(self, button):
        self._start_temp_screenlock_disable()

    def _start_temp_screenlock_disable(self):

        screenlock.disable()

        quantity = int(self._quantity_input.get_text())

        if self._unit_radio_group.get_active() == 'hours':
            minutes = 60 * quantity
        else:
            minutes = quantity

        self._seconds_left = minutes * 60
        self._update_time_left_label()
        self._reinit_main_ui_stack()

        GLib.timeout_add_seconds(
            priority=GLib.PRIORITY_DEFAULT,
            interval=1,
            function=self.update_seconds_left,
            data=None
        )

    def update_seconds_left(self, data=None):

        self._seconds_left -= 1
        self._update_time_left_label()

        self._update_lock_enabled()

        return self._seconds_left > 0

    def _update_lock_enabled(self):
        if self._seconds_left < 1:
            screenlock.enable()
            self._reinit_main_ui_stack()

    def _update_time_left_label(self):
        self._time_left_label.set_text(str(datetime.timedelta(seconds=self._seconds_left)))

    @property
    def _timer_ui(self):

        header_label = Gtk.Label()
        header_label.set_markup("<big><b>Screen lock disabled</b></big>")

        message_label = Gtk.Label.new("Enabling screen lock again after:")

        enable_now_button = Gtk.Button.new_with_label("Enable now")
        enable_now_button.connect('clicked', self._on_enable_now_button_click)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box.add(header_label)
        box.add(message_label)
        box.add(self._time_left_label)
        box.add(enable_now_button)

        return box

    def _on_enable_now_button_click(self, button):
        self._seconds_left = 0
        self._update_lock_enabled()

    @property
    def _already_disabled_ui(self):
        message = Gtk.Label.new("The screen lock is already disabled")
        message.set_halign(Gtk.Align.START)
        return message


class LockFreeApplication(Gtk.Application):

    def __init__(self, *args, **kwargs):

        super().__init__(
            *args,
            application_id='org.gitaarik.lockfree',
            **kwargs
        )

        self.window = None

    def do_startup(self):

        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new('about', None)
        action.connect('activate', self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.on_quit)
        self.add_action(action)

        self.connect('shutdown', self.on_shutdown)

        builder = Gtk.Builder.new_from_string(MENU_XML, -1)
        self.set_app_menu(builder.get_object('app-menu'))

        self.window = LockFreeWindow(application=self)

    def do_activate(self):
        self.window.present()

    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.present()

    def on_shutdown(self, action):
        screenlock.enable()

    def on_quit(self, action, param):
        self.quit()


if __name__ == '__main__':
    app = LockFreeApplication()
    app.run()
