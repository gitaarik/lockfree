# LockFree

GNOME desktop utility to temporarily disable the screen lock

![LockFree screenshot main screen](screenshots/main-screen.png)
![LockFree screenshot lock disabled timer](screenshots/lock-disabled-timer.png)


## Description

Sometimes it is annoying when your screen gets locked automatically. For
example, you're working at home (a safe environment) and you make a tea or
something, your screen get's locked and you have to re-enter your password to
unlock it. It takes extra time and it's especially annoying if it happens
multiple times a day.

Of course you can extend the time before your screen locks, but you might not
want this because it can be a security vulnerability. For example, when you're
not in a safe environment and you forgot to manually lock your screen when you
walked away. With a long screen lock delay time, your machine can be vulnerable
for a long time.

You can also manually disable the screen lock every time you are in a safe
environment. However, it takes quite some actions to do this; you have to
navigate to the right settings screen, change the appropriate setting, and
restore the old settings when you leave the safe environment. It can also be
dangerous if you forget to restore the old settings.

This utility makes it easy and safe. It will disable the screen lock for a
chosen time and automatically enables it again after that time. If you
accidentally close the program, it will also enable the screen lock again.


## Install

To install, you'll need [Python 3](https://www.python.org/) and
[Pipenv](https://pipenv.pypa.io/). Then you can install it using:

    git clone git@gitlab.com:gitaarik/lockfree.git
    cd lockfree
    pipenv install

Then you can build the binary with:

    pipenv run make

Then you will get a binary file `lockfree.bin` that you can run from the CLI:

    ./lockfree.bin


## Desktop integration

Make LockFree appear in your desktop application launcher, so you can easily
launch it.

1. Create a symlink to add LockFree to the `PATH` environment variable,
   assuming `~/.local/bin` is in your `PATH`.

        ln -s ~/path/to/lockfree.bin ~/.local/bin/lockfree

2. Copy the `lockfree.desktop` file to the applications folder so that GNOME
   will detect it.

        cp ~/path/to/lockfree.desktop ~/.local/share/applications

3. Now open this file and update the `Icon=lockfree.png` to point to the
   absolute path to the icon (inside the icons/ directory). For example:

        Icon=/path/to/lockfree/icons/lockfree.png


## How the app works

What this app does is change the GNOME screensaver settings using
[GSettings](https://developer.gnome.org/GSettings/). The settings of the
screensaver are namespaced under `org.gnome.desktop.screensaver` and the
setting that LockFree updates is `lock-enabled`. Normally, when you have the
screen lock enabled, the value of this settings is `True`. LockFree temporarily
sets this value to `False`. When the timer is finished, or the "Enable now"
button is pressed, or the program is closed, the setting is put back to `True`.

You can also manually change these settings with [dconf
Editor](https://wiki.gnome.org/Apps/DconfEditor). You can often find advanced
settings of programs that are not available from the UI of the program itself.
Another useful setting of the GNOME screensaver regarding screen lock is
`lock-delay`. When the screensaver kicks in, normally the screen is instantly
locked. With this setting you can set a delay time before the screen gets
locked. So when you see your screensaver kicks in and you don't want your
computer to be locked, you still have some time to hit the spacebar so your
screen won't be locked.


## Development

When you make changes to `lockfree.py`, you can check your changes by running
the application without compiling:

    # Activate the virtual environment
    pipenv shell

    # Install the dependencies and dev-dependencies
    pipenv install --dev

    # Run the program with python
    python lockfree.py

When you're happy with the changes, you can create a new binary with `make`. If
you haven't activated the virtual environment, you can use `pipenv run make`.
Check the `Makefile` to see what happens when you run `make`. It uses
[nuitka](http://nuitka.net/) to make a binary.

This project is made with [PyGObject](https://pygobject.readthedocs.io/). Check
their documentation to understand better how this application works.
