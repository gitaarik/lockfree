from gi.repository import Gtk


class RadioGroup(Gtk.Box):

    def __init__(self, labels, orientation=Gtk.Orientation.VERTICAL):

        super().__init__()

        self.set_orientation(orientation)
        self.radios = {}
        group = None

        for key, label in labels.items():

            radio = Gtk.RadioButton.new_with_label_from_widget(group, label)

            if not group:
                group = radio

            radio.connect('key-press-event', self.keypress)

            self.add(radio)
            self.radios[key] = radio

    def get_active(self):
        for key, radio in self.radios.items():
            if radio.get_active():
                return key

    def set_active(self, key, focus=False):

        self.radios[key].set_active(True)

        if focus:
            self.radios[key].grab_focus()

    def connect(self, event, callback):
        if event == 'activate':
            for radio in self.radios.values():
                radio.connect(event, callback)
        else:
            super().connect(event, callback)

    def keypress(self, radio, event_key):
        if event_key.string == 'j':
            self.move_active_down()
        elif event_key.string == 'k':
            self.move_active_up()

    def move_active_down(self):

        if self.active_index + 1 >= len(self.radios):
            return

        new_active_key = list(self.radios)[self.active_index + 1]
        self.set_active(new_active_key, focus=True)

    def move_active_up(self):

        if self.active_index < 1:
            return

        new_active_key = list(self.radios)[self.active_index - 1]
        self.set_active(new_active_key, focus=True)

    @property
    def active_index(self):
        return list(self.radios).index(self.get_active())
