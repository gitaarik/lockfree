from gi.repository import Gtk


class SpinButton(Gtk.SpinButton):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connect('key-press-event', self.keypress)
        self.connect('key-release-event', self.keypress)

    def keypress(self, input, event_key):
        if event_key.string == 'k':
            self.increase_value()
        elif event_key.string == 'j':
            self.decrease_value()
        else:
            self.restore_prev_value()

    def increase_value(self):
        value = self.get_value() + 1
        self.set_value(value)
        self.set_text(str(int(value)))

    def decrease_value(self):
        value = self.get_value() - 1
        self.set_value(value)
        self.set_text(str(int(value)))

    def restore_prev_value(self):
        value = self.get_value()
        self.set_value(value)
        self.set_text(str(int(value)))
